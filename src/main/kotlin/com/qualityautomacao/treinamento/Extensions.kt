package com.qualityautomacao.treinamento.extensions

import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JREmptyDataSource
import net.sf.jasperreports.engine.data.ListOfArrayDataSource
import javax.persistence.Tuple
import javax.persistence.TupleElement

fun List<Tuple>.toDataSource(): JRDataSource {
    if (isEmpty()) return JREmptyDataSource()

    return ListOfArrayDataSource(
        this.map { it.toArray() },
        this.first().elements.map { it.alias }.toTypedArray()
    )
}
// função pro tuple virar um map
fun Tuple.toMap(keyMapper: (String) -> String = { it }): MutableMap<String, Any> {
    val map = mutableMapOf<String, Any>()
    elements.forEach {
        map[keyMapper(it.alias)] = this[it.alias]
    }
    return map
}

// arquivo feito para criar funções para outros lugares
// Fazer transformações, tem um tipo e quer transformar em outro tipo