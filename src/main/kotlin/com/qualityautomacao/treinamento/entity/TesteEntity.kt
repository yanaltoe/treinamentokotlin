package com.qualityautomacao.treinamento.entity

import javax.persistence.*

@Entity
@Table(name = "teste", schema = "public", catalog = "treinamento")
class TesteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "idlanche")
    val idlanche: Int? = 0

    @Column(name = "tp_lanche")
    var tp_lanche: String = ""

    @Column(name = "valanche")
    var valanche: Double = 0.0

    @Column(name = "nome_lanche")
    var nome_lanche: String = ""

    @Column(name = "custo")
    var custo: Double = 0.0
}

