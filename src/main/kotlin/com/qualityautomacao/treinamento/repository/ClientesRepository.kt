package com.qualityautomacao.treinamento.repository

import com.qualityautomacao.treinamento.entity.ClientesEntity
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import java.time.LocalDate
import javax.persistence.Tuple

interface ClientesRepository : CrudRepository<ClientesEntity, Long> {
    @Query(
        value = """ select cli.cli_nome_a                                         as cliente,
                           count(distinct itm.pedido)                             as numero_vendas,
                           count(itm)                                             as numero_itens,
                           count(distinct abt.controle)                           as numero_abastecimento,                           
                           sum(itm.quant) filter (where abt.controle is not null) as total_litros,
                           sum(itm.quant) filter (where abt.controle is null)     as total_produto,
                           sum(itm.valitembruto)                                  as total_bruto,
                           sum(itm.valitem)                                       as total_liquido,
                           sum(itm.valitem) / count(distinct ped)                 as ticket_medio
                    from pedido ped 
                    inner join itemped itm on ped.codi = itm.pedido
                    inner join clientes cli on cli.cli_cod_a = ped.clie
                    left join abastecimentos abt on abt.controle = itm.idabast
                    where ped.dtfiscal between :DATA_INICIAL and :DATA_FINAL
                    group by cli_nome_a
                    order by ticket_medio desc""", nativeQuery = true
    )
    fun relatorioCliente(
        @Param("DATA_INICIAL") dataInicial: LocalDate,
        @Param("DATA_FINAL") dataFinal: LocalDate
    ): List<Tuple>

    @Query(
        value = """ select
                    cli.cli_nome_a as cliente,
                    coalesce(cast(last(ped.cupom) as varchar), 'SEM CUPOM') as cupom_ultimo_pedido,
                    coalesce(last(ped.valortotal), 0) as vl_ultimo_pedido,
                    last(case 
                    when pra.tpprazo = 'CHEQUE' then che.bompar
                    when pra.tpprazo = 'CARTÃO' then car.bompa
                    else ped.dtfiscal 
                    end) as ultimo_recebimento,
                    coalesce(sum(ped.valortotal), 0) as vl_total_comprado_cliente,
                    coalesce(sum(ped.valortotal) filter(where pra.tpprazo ='CHEQUE'), 0) as total_cheque,              
                    coalesce(sum(ped.valortotal) filter(where pra.tpprazo ='CARTAO'), 0) as total_cartao,
                    coalesce(sum(ped.valortotal) filter(where pra.tpprazo ='DINHEIRO'), 0) as total_dinheiro,
                    coalesce(count(pro.tpprod) filter(where tpprod ='PRODUTO'), 0) as qtd_produtos,
                    coalesce(sum(itm.quant), 0) as total_litros
                    from clientes cli
                    left join pedido ped on cli.cli_cod_a = ped.clie and ped.dtfiscal between '2021-01-01' and '2021-12-31'
                    left join itemped itm on ped.codi = itm.pedido
                    left join prazos pra on pra.codpra = ped.formapg
                    left join produtos pro on pro.codpro = itm.codpec
                    left join cheques che on ped.codi = che.docu
                    left join cartes car on ped.codi = car.docu
                    and pra.tpprazo in('CHEQUE', 'CARTAO','DINHEIRO')
                    group by cli.cli_nome_a
                    order by cliente""", nativeQuery = true
    )
    fun relatorioClienteUltimaVenda(
        @Param("DATA_INICIAL") dataInicial: LocalDate,
        @Param("DATA_FINAL") dataFinal: LocalDate
    ): List<Tuple>

    @Query(
        value = """select
        pro.nompro as nome_produto,
        count(*) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_A) as quantidade_ano_A,
        max(itm.valitem) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_A) as valor_ano_A,
        count(*) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_B) as quantidade_ano_B,
        max(itm.valitem) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_B) as valor_ano_B,
        max(itm.valitem) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_A) - max(itm.valitem) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_B) as diferenca,
        (max(itm.valitem) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_A) - max(itm.valitem) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_B)) * 100 / max(itm.valitem) filter (where cast(to_char(ped.dtem, 'YYYY') as int) = :ANO_B) as perc_diferenca
        from produtos pro
        inner join itemped itm on itm.codpec = pro.codpro
        inner join pedido ped on ped.codi = itm.pedido
        where pro.tpprod = 'PRODUTO'
        group by nome_produto""", nativeQuery = true
    )
    fun relatorioCurvaAbc(
        @Param("ANO_A") anoA: Int,
        @Param("ANO_B") anoB: Int
    ): List<Tuple>


}
