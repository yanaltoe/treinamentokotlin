package com.qualityautomacao.treinamento.repository

import com.qualityautomacao.treinamento.entity.TesteEntity
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.persistence.Tuple

@Repository
interface TesteRepository : CrudRepository<TesteEntity, Int> {
    @Query(
        value = """ select a, b, c
                    from teste tes
                    where tes.idlanche = ID """, nativeQuery = true
    )
    fun item(@Param("ID") id: Int): Tuple?

    @Query(
        value = """ select tes.idlanche, tp_lanche, valanche, nome_lanche, custo
                from teste tes """ , nativeQuery = true
    )
    fun lista(): List<Tuple>
}

