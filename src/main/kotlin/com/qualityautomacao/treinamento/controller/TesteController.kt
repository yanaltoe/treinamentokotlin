package com.qualityautomacao.treinamento.controller

import com.qualityautomacao.treinamento.entity.TesteEntity
import com.qualityautomacao.treinamento.extensions.toMap
import com.qualityautomacao.treinamento.repository.TesteRepository
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

// criar no postgres uma tabela com nome teste com uns 5 campo

@RestController                          // digo que essa classe tem requests dentro dela
@RequestMapping("teste")                                  // da um caminho pra todas as requisiçoes aqui dentro
class TesteController(private val testeRepository: TesteRepository) // construtor 98% das vezes, em detrimento do @inject lateinit
{

    @GetMapping                                                      // em geral usaremos mais get
    fun lista() : List<MutableMap<String, Any>> {
        return testeRepository.lista().map { it.toMap() }                    // crudge
    }

    @GetMapping("{id}")
    fun item(@PathVariable id: Int) : Map<String, Any>? {
        return testeRepository.item(id)?.toMap() ?: return null
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun incluir(@RequestBody teste: TesteEntity) {
        testeRepository.save(teste)
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun alterar(@PathVariable id: Int, @RequestBody parametros:TesteEntity) : String {
    val teste = testeRepository.findById(id).get()
    teste.apply {
        nome_lanche = parametros.nome_lanche
        valanche = parametros.valanche
    }
        return "Sucesso" }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun deletar(@PathVariable id: Int) {

    }
}